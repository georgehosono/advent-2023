#! /bin/python

FILE = "input"

MAX_RED = 12
MAX_BLUE = 14
MAX_GREEN = 13

game_ids = 0
total_power = 0

with open(FILE, "r") as f:
    for line in f:
        parts = line.split(": ")
        game_num = int(parts[0][5:])

        reds = 0
        greens = 0
        blues = 0

        draws = parts[1].replace(";", ",").strip()
        draws = draws.split(", ")
        for draw in draws:
            if "green" in draw:
                greens = max(greens, int(draw.split()[0]))
            elif "blue" in draw:
                blues = max(blues, int(draw.split()[0]))
            elif "red" in draw:
                reds = max(reds, int(draw.split()[0]))
            else:
                raise ValueError(draw)

        if reds > MAX_RED or greens > MAX_GREEN or blues > MAX_BLUE:
            pass
        else:
            game_ids += game_num

        total_power += reds * greens * blues

print(f"Part One: {game_ids}")
print(f"Part Two {total_power}")
