fn main() {
    // include_str compiles the input into the executable, which saves time
    // on opening the file and reading it
    let input = include_str!("../../input");
    // let input = include_str!("../../test");
    let part1 = part_one(input);
    let part1_readable = part_one_readable(input);
    assert_eq!(part1, part1_readable);
    println!("Part 1: {part1}");

    let part2 = part_two(input);
    println!("Part 2: {part2}");
}

// This solution avoids unneeded heap allocations, but it's far from the most readable.
fn part_one(input: &str) -> u32 {
    input
        // Iterate over the lines in the input
        .lines()
        .map(|line| {
            // Filter out the non-digit characters from each line
            let mut it = line.chars().filter(|ch| ch.is_ascii_digit());

            // Parse the last digit as an integer
            let low_digit: u32 = it.nth_back(0).unwrap().to_digit(10).unwrap();

            // Parse the first digit as an integer. Or if the first digit has been
            // consumed by the iterator, repeat the previous digit.
            let high_digit = it
                .nth(0)
                .unwrap_or_default()
                .to_digit(10)
                .unwrap_or(low_digit);

            // Return the sum of the digits
            10 * high_digit + low_digit
        })
        // Add up the digit for each line
        .sum()
}

// A more readable solution to part one, that involves allocating a new string for each line
fn part_one_readable(input: &str) -> u32 {
    input
        .lines()
        .map(|line| {
            line.chars()
                // Filter out things that aren't numbers
                .filter(|ch| ch.is_ascii_digit())
                // Here's the additional allocation
                .collect::<String>()
        })
        .map(|s| {
            // Since Rust strings are UTF-8 encoded, this is only ok
            // because we know that there is only ascii data in the String.
            let slice = s.as_bytes();

            // This avoids all the to_digit error checking,
            // but it might fail silently and cause unexpected problems
            let high_digit: u32 = (slice[0] - '0' as u8).into();
            let low_digit: u32 = (slice[slice.len() - 1] - '0' as u8).into();
            10 * high_digit + low_digit
        })
        .sum()
}

// Just replace every word digit with a real digit.
// We need to include the word a second time to account for things like "oneight"
fn part_two(input: &str) -> u32 {
    let mut s = input.to_string();
    s = s
        .replace("one", "one1one")
        .replace("two", "two2two")
        .replace("three", "three3three")
        .replace("four", "four4four")
        .replace("five", "five5five")
        .replace("six", "six6six")
        .replace("seven", "seven7seven")
        .replace("eight", "eight8eight")
        .replace("nine", "nine9nine");
    part_one(s.as_str())
}
