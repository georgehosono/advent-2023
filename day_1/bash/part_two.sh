#! /bin/bash

set -e

sed 's/one/one1one/g' $1 | \
sed 's/two/two2two/g' | \
sed 's/three/three3three/g' | \
sed 's/four/four4four/g' | \
sed 's/five/five5five/g' | \
sed 's/six/six6six/g' | \
sed 's/seven/seven7seven/g' | \
sed 's/eight/eight8eight/g' | \
sed 's/nine/nine9nine/g' | \
sed 's/[a-z]//g' | \
sed -E 's/(.)(.*)/\1 \1\2/g' | \
sed -E 's/(.).*(.)/\1\2/g' | \
awk '{s+=$1} END {print s}'
