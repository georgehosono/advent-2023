#! /bin/bash

set -e

sed 's/[a-z]//g' $1 | \
sed -E 's/(.)(.*)/\1 \1\2/g' | \
sed -E 's/(.).*(.)/\1\2/g' | \
awk '{s+=$1} END {print s}'
