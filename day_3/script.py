#! /bin/python

FILE = "test"

gears = []

def get_char(x, y):
    if x < 0 or x >= len(gears[0]) or y < 0 or y >= len(gears):
        return "."
    return gears[y][x]
        

with open(FILE, "r") as f:
    for line in f:
        gears.append(list(line))

total = 0
offsets = [(-1,-1), (-1,0),(-1,1),(0,-1),(0,1),(1,-1),(1,0),(1,1)]
for j in range(len(gears)):
    for i in range(len(gears[j])):
        part_num = False
        for offset in offsets:
            ch = get_char(i + offset[0], j + offset[1])
            if not ch.isdigit() and ch != ".":
                part_num = True
        if not part_num and gears[j][i].isdigit():
            gears[j][i] = "."
print("".join(("".join(line) for line in gears)))
