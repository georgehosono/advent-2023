fn main() {
    let input = include_str!("../../input");
    let grid = parse_input(input);

    let part1 = part_one(&grid);
    println!("Part One: {part1}");

    let part2 = part_two(&grid);
    println!("Part Two: {part2}");
}

fn parse_input(input: &str) -> Vec<Vec<char>> {
    input.lines().map(|l| l.chars().collect()).collect()
}

fn get_at<T>(grid: &[Vec<T>], x: i32, y: i32) -> Option<&T> {
    if grid.is_empty()
        || y < 0
        || y >= grid.len() as i32
        || x < 0
        || x >= grid[y as usize].len() as i32
    {
        None
    } else {
        Some(&grid[y as usize][x as usize])
    }
}

fn get_at_mut<T>(grid: &mut Vec<Vec<T>>, x: i32, y: i32) -> Option<&T> {
    if grid.is_empty()
        || y < 0
        || y >= grid.len() as i32
        || x < 0
        || x >= grid[y as usize].len() as i32
    {
        None
    } else {
        Some(&grid[y as usize][x as usize])
    }
}

fn part_one(grid: &[Vec<char>]) -> u32 {
    // Make a copy of the entire grid so we can modify it
    let mut new_grid: Vec<Vec<(char, bool)>> = grid
        .iter()
        .map(|l| {
            l.iter()
                .map(|&ch| {
                    if !ch.is_ascii_digit() && ch != '.' {
                        (ch, true)
                    } else {
                        (ch, false)
                    }
                })
                .collect()
        })
        .collect();

    let offsets: [(i32, i32); 8] = [
        (-1, -1),
        (-1, 0),
        (-1, 1),
        (0, -1),
        (0, 1),
        (1, -1),
        (1, 0),
        (1, 1),
    ];

    // I think it's ok to only run this loop 3 times on my input
    // but is that's not a ~general solution~
    for _ in 0..new_grid[0].len() {
        for y in 0..new_grid.len() {
            for x in 0..new_grid[y].len() {
                if !new_grid[y][x].0.is_ascii_digit() {
                    continue;
                }
                let mut is_part_num = false;
                for (dx, dy) in offsets {
                    if let Some(&(_, true)) =
                        get_at_mut(&mut new_grid, x as i32 + dx, y as i32 + dy)
                    {
                        is_part_num = true;
                    }
                }
                if is_part_num {
                    new_grid[y][x].1 = true;
                }
            }
        }
    }

    let strings: Vec<String> = new_grid
        .iter()
        .map(|line| {
            line.iter()
                .map(|&(ch, ok)| if ok && ch.is_ascii_digit() { ch } else { '.' })
                .collect()
        })
        .collect();

    strings
        .iter()
        .flat_map(|line| {
            line.split('.')
                .map(|s| s.parse::<u32>().unwrap_or_default())
        })
        .sum()
}

fn chars_to_num(v: Vec<char>) -> Option<usize> {
    v.iter().collect::<String>().parse().ok()
}

fn get_nums_from_row(grid: &[Vec<char>], x: usize, row: usize) -> Vec<usize> {
    let mut nums = Vec::new();
    if get_at(grid, x as i32, row as i32).is_none() {
        return nums;
    }

    let (first, last) = grid[row].split_at(x);

    // There's one big number in the row
    if last[0].is_ascii_digit() {
        let mut row_num = Vec::<char>::new();
        for ch in first.iter().rev() {
            if ch.is_ascii_digit() {
                row_num.insert(0, *ch);
            } else {
                break;
            }
        }
        for ch in last.iter() {
            if ch.is_ascii_digit() {
                row_num.push(*ch);
            } else {
                break;
            }
        }
        if let Some(n) = chars_to_num(row_num) {
            nums.push(n)
        }
    } else {
        // there might be digits in the top corners
        if first[first.len() - 1].is_ascii_digit() {
            let mut left_num = Vec::new();
            for ch in first.iter().rev() {
                if ch.is_ascii_digit() {
                    left_num.insert(0, *ch);
                } else {
                    break;
                }
            }
            if let Some(n) = chars_to_num(left_num) {
                nums.push(n);
            }
        }
        if let Some(ch) = last.get(1) {
            if ch.is_ascii_digit() {
                let mut right_num = Vec::new();
                let mut it = last.iter();
                it.next(); //consume the first digit since we know its bad
                for ch in it {
                    if ch.is_ascii_digit() {
                        right_num.push(*ch);
                    } else {
                        break;
                    }
                }
                if let Some(n) = chars_to_num(right_num) {
                    nums.push(n);
                }
            }
        }
    }
    nums
}

fn get_adjacent_nums(grid: &[Vec<char>], x: usize, y: usize) -> Vec<usize> {
    let mut nums = Vec::new();
    nums.append(&mut get_nums_from_row(grid, x, y - 1));
    nums.append(&mut get_nums_from_row(grid, x, y + 1));

    let mut left_num = Vec::new();
    for i in (0..x).rev() {
        if grid[y][i].is_ascii_digit() {
            left_num.insert(0, grid[y][i])
        } else {
            break;
        }
    }
    if let Some(n) = chars_to_num(left_num) {
        nums.push(n);
    }

    let mut right_num = Vec::new();
    for i in (x + 1)..grid[y].len() {
        if grid[y][i].is_ascii_digit() {
            right_num.push(grid[y][i])
        } else {
            break;
        }
    }
    if let Some(n) = chars_to_num(right_num) {
        nums.push(n);
    }
    nums
}

fn part_two(grid: &Vec<Vec<char>>) -> usize {
    let mut count = 0;
    for j in 0..grid.len() {
        for i in 0..grid[j].len() {
            if grid[j][i] != '*' {
                continue;
            }
            let adjacent_nums = get_adjacent_nums(grid, i, j);
            if adjacent_nums.len() == 2 {
                count += adjacent_nums[0] * adjacent_nums[1];
            }
        }
    }
    count
}
