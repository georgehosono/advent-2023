fn main() {
    let input = include_str!("../../input");
    let (seeds, maps) = parse_input(input);
    let part1 = part_one(seeds.iter().copied(), &maps);
    println!("Part One: {part1}");

    let part2 = part_two(seeds, &maps);
    println!("Part Two: {part2}");
}

fn parse_input(input: &str) -> (Vec<usize>, Vec<Vec<[usize; 3]>>) {
    let mut it = input.split("\n\n");
    let s = it.next().expect("no seeds");
    let seeds = s
        .split_whitespace()
        .skip(1)
        .map(|s| s.parse().expect("could not parse int"))
        .collect();

    let maps = it
        .map(|block| {
            block
                .lines()
                .skip(1)
                .map(|l| {
                    let mut it = l.split_whitespace();
                    let dst = it.next().unwrap().parse().unwrap();
                    let src = it.next().unwrap().parse().unwrap();
                    let step = it.next().unwrap().parse().unwrap();
                    [dst, src, step]
                })
                .collect()
        })
        .collect();

    (seeds, maps)
}

fn get_next(idx: usize, maps: &[[usize; 3]]) -> usize {
    for map in maps.iter() {
        if idx >= map[1] && idx < map[1] + map[2] {
            return map[0] + (idx - map[1]);
        }
    }
    idx
}

fn part_one(seeds: impl Iterator<Item = usize>, maps: &[Vec<[usize; 3]>]) -> usize {
    seeds
        .map(|seed| maps.iter().fold(seed, |prev, map| get_next(prev, map)))
        .min()
        .expect("no seeds")
}

// This works, it just takes like 5 min to run on my laptop
// Why work smarter when you could work harder?
fn part_two(seeds: Vec<usize>, maps: &[Vec<[usize; 3]>]) -> usize {
    let it = seeds.chunks(2).flat_map(|arr| arr[0]..(arr[0] + arr[1]));
    part_one(it, maps)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_range() {
        let maps = vec![vec![[50, 98, 2], [52, 50, 48]]];
        assert_eq!(get_next(0, &maps[0]), 0);
        assert_eq!(get_next(1, &maps[0]), 1);

        assert_eq!(get_next(48, &maps[0]), 48);
        assert_eq!(get_next(49, &maps[0]), 49);
        assert_eq!(get_next(50, &maps[0]), 52);
        assert_eq!(get_next(51, &maps[0]), 53);

        assert_eq!(get_next(96, &maps[0]), 98);
        assert_eq!(get_next(97, &maps[0]), 99);
        assert_eq!(get_next(98, &maps[0]), 50);
        assert_eq!(get_next(99, &maps[0]), 51);
    }
}
