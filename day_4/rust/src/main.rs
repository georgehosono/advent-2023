use std::collections::BTreeSet;

fn main() {
    let input = include_str!("../../input");
    let part1 = part_one(input);
    println!("Part One: {part1}");

    let part2 = part_two(input);
    println!("Part Two: {part2}");
}

// get the number of elements in both the card and the winning number
fn parse_input(input: &str) -> impl Iterator<Item = usize> + '_ {
    input.lines().map(|l| {
        let data = l.split(':').nth(1).expect("bad line");
        let [my_nums, winning_nums]: [BTreeSet<&str>; 2] = data
            .split(" | ")
            .map(|part| part.split_whitespace().collect::<BTreeSet<&str>>())
            .collect::<Vec<BTreeSet<&str>>>()
            .try_into()
            .expect("could not collect");
        my_nums.intersection(&winning_nums).count()
    })
}

fn part_one(input: &str) -> usize {
    parse_input(input)
        .map(|wins| if wins > 0 { 1 << (wins - 1) } else { 0 })
        .sum()
}

fn part_two(input: &str) -> usize {
    let mut cards: Vec<(usize, usize)> = parse_input(input).map(|n| (1, n)).collect();
    for i in 0..cards.len() {
        for j in 0..cards[i].1 {
            let bonus_cards = cards[i].0;
            if let Some((copies, _)) = cards.get_mut(i + j + 1) {
                *copies += bonus_cards;
            }
        }
    }
    cards.iter().map(|(copies, _)| copies).sum()
}
