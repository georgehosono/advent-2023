fn main() {
    let input = include_str!("../../input");
    let part1 = part_one(input);
    println!("Part 1: {part1}");

    let part2 = part_two(input);
    println!("Part 2: {part2}");
}

fn get_distance(wait_time: usize, total_time: usize) -> usize {
    (total_time - wait_time) * wait_time
}

fn part_one(input: &str) -> usize {
    let mut it = input.lines().map(|l| {
        let mut it = l.split_whitespace();
        it.next();
        it.map(|s| s.parse::<usize>().expect("could not parse"))
            .collect::<Vec<_>>()
    });
    let times = it.next().expect("no times");
    let distances = it.next().expect("no distances");
    let races: Vec<_> = times
        .iter()
        .copied()
        .zip(distances.iter().copied())
        .collect();
    races
        .iter()
        .map(|(time, distance)| {
            (0..*time)
                .map(|t| get_distance(t, *time))
                .filter(|t| t > distance)
                .count()
        })
        .fold(1, |state, result| state * result)
}

// I'm shocked a naive solution is fast here
fn part_two(input: &str) -> usize {
    let v = input
        .lines()
        .map(|l| {
            let s: String = l.split_whitespace().skip(1).collect();
            s.parse::<usize>().expect("could not parse")
        })
        .collect::<Vec<_>>();
    let time = v[0];
    let distance = v[1];

    (0..time)
        .filter(|&i| get_distance(i, time) > distance)
        .count()
}
